FROM debian:jessie

# Give the container an id. Then salt can uniquely identify.
ARG id

RUN apt-get update
RUN apt-get -y install software-properties-common git wget

# Install salt
RUN wget -q -O- "http://debian.saltstack.com/debian-salt-team-joehealy.gpg.key" | apt-key add -
RUN echo "deb http://debian.saltstack.com/debian jessie-saltstack main" >> /etc/apt/sources.list.d/salt.list
RUN apt-get update && apt-get install -y salt-minion

# Create directory for holding config file to override default salt minion config
RUN mkdir -p /etc/salt/minion.d

# Override the default id with the id passed into the build context
RUN echo "id: ${id}" >> /etc/salt/minion.d/salt.conf

# Run salt in masterless mode in this container 
RUN echo "file_client: local" >> /etc/salt/minion.d/salt.conf

# Add the salt configs
ADD salt /srv/salt 

# Run the high state in this container
RUN salt-call state.highstate
