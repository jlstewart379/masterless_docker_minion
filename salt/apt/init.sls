git:
  pkg.latest
    
vim:
  pkg.latest
    
python3:
  pkg.latest
    
python3-dev:
  pkg.latest
    
python3-pip:
  pkg.latest
